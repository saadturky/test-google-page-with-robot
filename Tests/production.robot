*** Settings ***
Library    SeleniumLibrary
Suite Teardown  Close Browser


*** Test Cases ***
Test Chrome
    Open Browser        https://jaxber.com/      Chrome      options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors"); add_argument("--disable-dev-shm-usage"); add_argument("--no-sandbox"); add_argument("--headless")
    Set Selenium Speed  2
    Location Should Be   https://jaxber.com/en/
    Title Should Be     Jaxber
    [Teardown]          Close All Browsers
